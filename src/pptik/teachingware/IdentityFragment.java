package pptik.teachingware;

import pptik.teachingware.TeachingMaterial.IdentityInformation;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IdentityFragment extends Fragment {

	private TeachingMaterial content;
	
	public ViewGroup viewGroup;
	
	public IdentityFragment() {
		
	}

	public void setContent(TeachingMaterial content){
		this.content = content;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO: read the information of Teaching Material
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		viewGroup = (ViewGroup) inflater.inflate(
				R.layout.activity_main, container, false);
		for(final IdentityInformation identityElement : IdentityInformation.values()){
			int id = viewGroup.getResources().getIdentifier("label_" + identityElement.getName(), "id", viewGroup.getContext().getPackageName());
			TextView label = (TextView) viewGroup.findViewById(id);
			label.setTypeface(Typeface.createFromAsset(viewGroup.getContext().getAssets(), "fonts/Helvetica_Light_Normal.ttf"));
			label.setTextSize(25);
			
			ViewGroup parent = (ViewGroup) label.getParent();
			parent.setBackgroundColor(Color.argb(60, 255, 255, 255));
			
			TextView arrow = (TextView) parent.getChildAt(1);
			arrow.setTypeface(Typeface.createFromAsset(viewGroup.getContext().getAssets(), "fonts/Helvetica_Light_Normal.ttf"));
			arrow.setTextSize(25);
			
			parent.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					int id = viewGroup.getResources().getIdentifier("value_" + identityElement.getName(), "id", viewGroup.getContext().getPackageName());
					TextView value = (TextView) viewGroup.findViewById(id);
					expandOrCollapse(value, value.getVisibility() == View.VISIBLE? "collapse" : "expand");
				}
			});
			int value_id = viewGroup.getResources().getIdentifier("value_" + identityElement.getName(), "id", viewGroup.getContext().getPackageName());
			TextView value = (TextView) viewGroup.findViewById(value_id);
			value.setText(content.getIdentityField(identityElement));
			value.setTypeface(Typeface.createFromAsset(viewGroup.getContext().getAssets(), "fonts/Helvetica_Light_Normal.ttf"));
			value.setBackgroundColor(Color.argb(20, 255, 255, 255));
			value.setVisibility(View.GONE);
			value.setTextSize(20);
		}
		
		return viewGroup;
	}
	
	public void expandOrCollapse(final View v, String exp_or_colpse) {
	    if(exp_or_colpse.equals("expand"))
	    {
	    	v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
	        final int targetHeight = v.getMeasuredHeight();

	        v.setLayoutParams(new LinearLayout.LayoutParams(v.getLayoutParams().width, 0));
	        v.setVisibility(View.INVISIBLE);
	        Animation a = new Animation()
	        {
	        	@Override
	        	protected void applyTransformation(float interpolatedTime,
	        			Transformation t) {	        		
	        		v.setLayoutParams(new LinearLayout.LayoutParams(v.getLayoutParams().width, interpolatedTime == 1
	                        ? LayoutParams.WRAP_CONTENT
	                        : (int)(targetHeight * interpolatedTime)));
	        		if(interpolatedTime == 1){
	        			v.setVisibility(View.VISIBLE);
	        		}else{
	        			v.setVisibility(View.INVISIBLE);
	        		}
	                v.requestLayout();
	        	}

	            @Override
	            public boolean willChangeBounds() {
	                return true;
	            }
	        };

	        // 1dp/ms
	        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density * 4));
	        v.startAnimation(a);
	    }
	    else{
	    	final int initialHeight = v.getMeasuredHeight();

	        Animation a = new Animation()
	        {
	            @Override
	            protected void applyTransformation(float interpolatedTime, Transformation t) {
	                if(interpolatedTime == 1){
	                    v.setVisibility(View.GONE);
	                }else{
	                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
	                    v.requestLayout();
	                }
	            }

	            @Override
	            public boolean willChangeBounds() {
	                return true;
	            }
	        };

	        // 1dp/ms
	        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density * 4));
	        v.startAnimation(a);
	    }
	}
}