package pptik.teachingware;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {

	private String id;
	private String group_id;
	private String status;
	private String title;
	private String description;
	private String school;
	private String topic;
	private String theme;
	private String grade;
	private String semester;
	private String steps;
	private String created_at;
	private String updated_at;
	
	public JSONParser(String s) throws JSONException{
		JSONObject obj = new JSONObject(s);
		id =  obj.getString("id");
		group_id =  obj.getString("group_id");
		status =  obj.getString("status");
		title =  obj.getString("title");
		description =  obj.getString("description");
		school =  obj.getString("school");
		topic =  obj.getString("topic");
		theme =  obj.getString("theme");
		grade =  obj.getString("grade");
		semester =  obj.getString("semester");
		steps =  obj.getString("steps");
		created_at =  obj.getString("created_at");
		updated_at =  obj.getString("updated_at");
	}//end ctor
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getSteps() {
		return steps;
	}

	public void setSteps(String steps) {
		this.steps = steps;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	
}//end class

/* contoh jsonnya:
{
	"id":20,
	"group_id":2,
	"status":0,
	"title":"Game Deveopment Life Cycle",
	"description":"Tahap-tahap membuat game dan daur hidup sebuah proyek pengembangan game.",
	"school":"SD Pengangguran",
	"topic":"Social Science",
	"theme":"Computer",
	"grade":5,
	"semester":2,
	"steps":4,
	"created_at":"2014-10-14 08:34:50",
	"updated_at":"2014-10-14 08:34:50",
	"authors":[
		{
			"id":2,
			"group_id":2,
			"username":"cesarb",
			"email":"cesar.bryan@example.com",
			"full_name":"Cesar Bryan",
			"role":"user",
			"created_at":"2014-10-09 06:49:02",
			"updated_at":"2014-10-09 06:49:02"
		},
		{
			"id":1,
			"group_id":2,
			"username":"greggk",
			"email":"gregg.king@example.com",
			"full_name":"Gregg King",
			"role":"user",
			"created_at":"2014-10-09 06:49:02",
			"updated_at":"2014-10-09 06:49:02"
		}
	],
	"activities":[
		{
			"id":30,
			"material_id":20,
			"resource_id":0,
			"index_no":1,
			"description":"<p>Salam, buka, dan jelaskan pentingnya mengapa ada proyek game.</p>\r\n",
			"created_at":"2014-10-14 08:34:50",
			"updated_at":"2014-10-14 08:34:50"
		},
		{
			"id":31,
			"material_id":20,
			"resource_id":0,
			"index_no":2,
			"description":"<p>Sebuah proyek game dilakukan dengan pembuatan grup dan brainstorming. Detil dapat dilihat pada presentasi resource.</p>\r\n","created_at":"2014-10-14 08:34:50","updated_at":"2014-10-14 08:34:50"},{"id":32,"material_id":20,"resource_id":0,"index_no","description":"<p>Berikan PR membuat game. Rencana pengembangan dikumpulkan minggu depan.</p>\r\n",
			"created_at":"2014-10-14 08:34:50",
			"updated_at":"2014-10-14 08:34:50"
		}
	]
}

{"id":20,"group_id":2,"status":0,"title":"Game Deveopment Life Cycle","description":"Tahap-tahap membuat game dan daur hidup sebuah proyek pengembangan game.","school":"SD Pengangguran","topic":"Social Science","theme":"Computer","grade":5,"semester":2,"steps","created_at":"2014-10-14 08:34:50","updated_at":"2014-10-14 08:34:50","authors":[{"id":2,"group_id":2,"username":"cesarb","email":"cesar.bryan@example.com","full_name":"Cesar Bryan","role":"user","created_at":"2014-10-09 06:49:02","updated_at":"2014-10-09 06:49:02"},{"id":10,"group_id":2,"username":"greggk","email":"gregg.king@example.com","full_name":"Gregg King","role":"user","created_at":"2014-10-09 06:49:02","updated_at":"2014-10-09 06:49:02"}],"activities":[{"id":30,"material_id":20,"resource_id":0,"index_no":1,"description":"<p>Salam, buka, dan jelaskan pentingnya mengapa ada proyek game.</p>\r\n","created_at":"2014-10-14 08:34:50","updated_at":"2014-10-14 08:34:50"},{"id":31,"material_id":20,"resource_id":0,"index_no":2,"description":"<p>Sebuah proyek game dilakukan dengan pembuatan grup dan brainstorming. Detil dapat dilihat pada presentasi resource.</p>\r\n","created_at":"2014-10-14 08:34:50","updated_at":"2014-10-14 08:34:50"},{"id":32,"material_id":20,"resource_id":0,"index_no":3,"description":"<p>Berikan PR membuat game. Rencana pengembangan dikumpulkan minggu depan.</p>\r\n","created_at":"2014-10-14 08:34:50","updated_at":"2014-10-14 08:34:50"}]}
*/