package pptik.teachingware;

import java.io.File;
import java.io.IOException;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

public class PresentationActivity extends Activity {
	
	// Player
	VideoView videoView;
	ImageView imageView;
	WebView webView;
	MediaPlayer mediaPlayer;
	MediaController mediaController;
	
	// Assets
	Bitmap image;
	Bitmap soundIcon;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_presentation);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		loadAssets();
		videoView = new VideoView(getApplicationContext());
		imageView = new ImageView(getApplicationContext());
		webView = new WebView(getApplicationContext());
		mediaPlayer = new MediaPlayer();
		
	}
	
	@Override
	protected void onStart() {
		String mode = getIntent().getStringExtra("mode");
		String mediaLocation = getIntent().getStringExtra("media_location");
		if(mode.compareTo("image") == 0){
			openImage(mediaLocation);
		}else if(mode.compareTo("video") == 0){
			openVideo(mediaLocation);
		}else if(mode.compareTo("audio") == 0){
			openAudio(mediaLocation);
		}else if(mode.compareTo("animation") == 0){
			openAnimation(mediaLocation);
		}
		super.onStart();
	}
	
	@Override
	protected void onDestroy() {
		mediaPlayer.stop();
		webView.loadUrl("");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.presentation, menu);
		return true;
	}
	
	public void openImage(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) findViewById(R.id.current_content);
		layout.removeAllViews();
		image = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation);
		if (image != null) {
			imageView.setImageBitmap(image);
		}
		layout.addView(imageView);
		imageView.getLayoutParams().height = LayoutParams.MATCH_PARENT;
		imageView.getLayoutParams().width = LayoutParams.MATCH_PARENT;
	}

	public void openAudio(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) findViewById(R.id.current_content);
		layout.removeAllViews();
		if (soundIcon != null) {
			imageView.setImageBitmap(soundIcon);
			imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					showController();
				}
			});
		}
		layout.addView(imageView);
		try {
			File sound = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation);
			Uri uri = Uri.fromFile(sound);
			mediaPlayer = MediaPlayer.create(
					getApplicationContext(),
					uri);
			mediaPlayer.start();
			mediaController = new MediaController(this);
			mediaController.setMediaPlayer(new MediaController.MediaPlayerControl() {
				
				@Override
				public void start() {
					mediaPlayer.start();
				}
				
				@Override
				public void seekTo(int value) {
					mediaPlayer.seekTo(value);
				}
				
				@Override
				public void pause() {
					mediaPlayer.pause();
				}
				
				@Override
				public boolean isPlaying() {
					return mediaPlayer.isPlaying();
				}
				
				@Override
				public int getDuration() {
					return mediaPlayer.getDuration();
				}
				
				@Override
				public int getCurrentPosition() {
					return mediaPlayer.getCurrentPosition();
				}
				
				@Override
				public int getBufferPercentage() {
					return 0;
				}
				
				@Override
				public int getAudioSessionId() {
					return mediaPlayer.getAudioSessionId();
				}
				
				@Override
				public boolean canSeekForward() {
					return true;
				}
				
				@Override
				public boolean canSeekBackward() {
					return true;
				}
				
				@Override
				public boolean canPause() {
					return true;
				}
			});
			mediaController.setAnchorView(imageView);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void openVideo(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) findViewById(R.id.current_content);
		layout.removeAllViews();
		layout.addView(videoView);
		String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation;
		File video = new File(fileName);
		videoView.setVideoURI(Uri.fromFile(video));
		mediaController = new MediaController(this);
		mediaController.setAnchorView(videoView);
		videoView.setMediaController(mediaController);
		videoView.start();
	}

	@SuppressWarnings("deprecation")
	public void openAnimation(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) findViewById(R.id.current_content);
		layout.removeAllViews();
		layout.addView(webView);
		String fileName = Environment.getExternalStorageDirectory() + "/" + mediaLocation;
		webView.getSettings().setPluginState(PluginState.ON);
		webView.loadUrl("file:///" + fileName);
		webView.getLayoutParams().height = LayoutParams.MATCH_PARENT;
		webView.getLayoutParams().width = LayoutParams.MATCH_PARENT;
	}

	@Override
	public void onBackPressed() {
		stopAllMedia();
		super.onBackPressed();
	}
	
	private void loadAssets() {
		try {
			image = BitmapFactory.decodeStream(getAssets().open("example.jpg"));
			soundIcon = BitmapFactory.decodeStream(getAssets().open("soundicon.jpg"));
		} catch (IOException e) {
			return;
		}
	}

	private void stopAllMedia() {
		if (mediaPlayer.isPlaying())
			mediaPlayer.stop();
		webView.loadUrl("");
	}

	public void showController() {
		if (mediaController != null) {
			mediaController.show();
		}
	}
}
