package pptik.teachingware;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import pptik.teachingware.ScenarioStep.MediaType;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebSettings.PluginState;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class ScenarioFragment extends Fragment {

	// RPP Object (Scenario)
	private TeachingMaterial content;
	
	// Player
	VideoView videoView;
	ImageView imageView;
	WebView webView;
	MediaPlayer mediaPlayer;
	MediaController mediaController;

	// Assets
	Bitmap image;
	Bitmap soundIcon;

	//ViewGroup
	ViewGroup viewGroup;
	
	public ScenarioFragment() {
		
		mediaPlayer = new MediaPlayer();
	}
	
	public void setContent(TeachingMaterial content){
		this.content = content;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		loadAssets();
		
		// Initialize all views
		videoView = new VideoView(getActivity().getApplicationContext());
		imageView = new ImageView(getActivity().getApplicationContext());
		webView = new WebView(getActivity().getApplicationContext());

		super.onCreate(savedInstanceState);
		
		// fill listRpp
		/*ScenarioStep rpp1 = new ScenarioStep("Guru memberi salam", "", MediaType.NONE);
		ScenarioStep rpp2 = new ScenarioStep("Guru menampilkan gambar", "example.jpg", MediaType.IMAGE);
		ScenarioStep rpp3 = new ScenarioStep("Guru memainkan suara", "suara", MediaType.AUDIO);
		ScenarioStep rpp4 = new ScenarioStep("Guru menampilkan video", "video", MediaType.VIDEO);
		ScenarioStep rpp5 = new ScenarioStep("Guru menampilkan animasi", "animasi", MediaType.ANIMATION);
		ScenarioStep rpp6 = new ScenarioStep("Guru menampilkan ppt", "ppt", MediaType.PPT);
		ScenarioStep rpp7 = new ScenarioStep("Guru menampilkan pdf", "pdf", MediaType.PDF);
		
		content.addScenarioStep(rpp1);
		content.addScenarioStep(rpp2);
		content.addScenarioStep(rpp3);
		content.addScenarioStep(rpp4);
		content.addScenarioStep(rpp5);
		content.addScenarioStep(rpp6);
		content.addScenarioStep(rpp7);
		
		*/
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		viewGroup = (ViewGroup) inflater.inflate(
				R.layout.activity_exercise, container, false);
		
		LinearLayout layoutDescription = (LinearLayout) viewGroup.findViewById(R.id.deskripsirpp);
		layoutDescription.removeAllViews();
		
		for (int i = 0; i < content.getCountStep(); i++) {
			LinearLayout layoutPoint = new LinearLayout(getActivity().getApplicationContext());
			layoutPoint.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 200));
			layoutPoint.setOrientation(LinearLayout.HORIZONTAL);
			layoutPoint.setGravity(Gravity.LEFT);
			
			TextView numText = new TextView(getActivity());
			numText.setText("" + (i + 1));
			numText.setTypeface(Typeface.createFromAsset(viewGroup.getContext().getAssets(), "fonts/Helvetica_Light_Normal.ttf"));
			numText.setTextColor(Color.WHITE);
			numText.setGravity(Gravity.CENTER);
			numText.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1f));
			numText.setTextSize(32);
			layoutPoint.addView(numText);
			
			LinearLayout layoutContent = new LinearLayout(getActivity().getApplicationContext());
			layoutContent.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 8f));
			layoutContent.setOrientation(LinearLayout.VERTICAL);
			layoutContent.setGravity(Gravity.LEFT);
			
			TextView teks = new TextView(getActivity());
			teks.setText(content.getStep(i).getDescription());
			teks.setTypeface(Typeface.createFromAsset(viewGroup.getContext().getAssets(), "fonts/Helvetica_Light_Normal.ttf"));
			teks.setTextColor(Color.WHITE);
			teks.setGravity(Gravity.CENTER_VERTICAL);
			teks.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 150));
			teks.setTextSize(22);
			layoutContent.addView(teks);
			
			

			// For step that contains media
			if (content.getStep(i).getMediaType() != MediaType.NONE) {

				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				layoutParams.gravity = Gravity.RIGHT;
				
				LinearLayout layoutButton = new LinearLayout(getActivity().getApplicationContext());
				layoutButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				layoutButton.setOrientation(LinearLayout.HORIZONTAL);
				layoutButton.setGravity(Gravity.RIGHT);
				
				// Temporary button
				Button previewButton = new Button(getActivity());
				if(content.getStep(i).getMediaType() != MediaType.PDF && content.getStep(i).getMediaType() != MediaType.PPT){
					previewButton.setBackgroundResource(R.drawable.play);
					previewButton.setHeight(50);
					previewButton.setWidth(30);
					previewButton.setLayoutParams(layoutParams);
					previewButton.setTextColor(Color.BLACK);
					previewButton.setGravity(Gravity.RIGHT);
					layoutButton.addView(previewButton);
				}
				
				Button showButton = new Button(getActivity());
				showButton.setBackgroundResource(R.drawable.show);
				showButton.setHeight(50);
				showButton.setWidth(30);
				showButton.setLayoutParams(layoutParams);
				showButton.setTextColor(Color.BLACK);
				showButton.setGravity(Gravity.RIGHT);
				layoutButton.addView(showButton);

				layoutContent.addView(layoutButton);
				
				final String mediaLocation = content.getStep(i).getMediaLocation();
				// Set Listener to the button, depend on contents
				if (content.getStep(i).getMediaType() == MediaType.IMAGE) { 
					previewButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View arg0) {
							previewImage(mediaLocation);
						}
					});
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showImage(mediaLocation);
						}
					});
				} else if (content.getStep(i).getMediaType() == MediaType.VIDEO) {
					previewButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View arg0) {
							previewVideo(mediaLocation);
						}
					});
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showVideo(mediaLocation);
						}
					});
				} else if (content.getStep(i).getMediaType() == MediaType.ANIMATION) {
					previewButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View arg0) {
							previewAnimation(mediaLocation);
						}
					});
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showAnimation(mediaLocation);
						}
					});
				} else if (content.getStep(i).getMediaType() == MediaType.AUDIO) {
					previewButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View arg0) {
							previewAudio(mediaLocation);
						}
					});
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showAudio(mediaLocation);
						}
					});
				} else if(content.getStep(i).getMediaType() == MediaType.PDF){
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showPDF(mediaLocation);
						}
					});
				} else if(content.getStep(i).getMediaType() == MediaType.PPT){
					showButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							showPPT(mediaLocation);
						}
					});
				}
			}
			
			layoutPoint.addView(layoutContent);
			layoutDescription.addView(layoutPoint);

			LinearLayout divider = new LinearLayout(getActivity());
			divider.setBackgroundColor(Color.LTGRAY);
			divider.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT, 1));
			layoutDescription.addView(divider);

		}
		
		return viewGroup;
	}

	public void previewImage(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) viewGroup.findViewById(R.id.current_content);
		layout.removeAllViews();
		image = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation);
		if (image != null) {
			imageView.setImageBitmap(image);
		}
		layout.addView(imageView);
		imageView.getLayoutParams().height = LayoutParams.MATCH_PARENT;
		imageView.getLayoutParams().width = LayoutParams.MATCH_PARENT;
	}

	public void previewAudio(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) viewGroup.findViewById(R.id.current_content);
		layout.removeAllViews();
		if (soundIcon != null) {
			imageView.setImageBitmap(soundIcon);
			imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					showController();
				}
			});
		}
		layout.addView(imageView);
		try {
			File sound = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation);
			Uri uri = Uri.fromFile(sound);
			mediaPlayer = MediaPlayer.create(
					getActivity().getApplicationContext(),
					uri);
			mediaPlayer.start();
			mediaController = new MediaController(getActivity());
			mediaController
					.setMediaPlayer(new MediaController.MediaPlayerControl() {

						@Override
						public void start() {
							mediaPlayer.start();
						}

						@Override
						public void seekTo(int value) {
							mediaPlayer.seekTo(value);
						}

						@Override
						public void pause() {
							mediaPlayer.pause();
						}

						@Override
						public boolean isPlaying() {
							return mediaPlayer.isPlaying();
						}

						@Override
						public int getDuration() {
							return mediaPlayer.getDuration();
						}

						@Override
						public int getCurrentPosition() {
							return mediaPlayer.getCurrentPosition();
						}

						@Override
						public int getBufferPercentage() {
							return 0;
						}

						@Override
						public int getAudioSessionId() {
							return mediaPlayer.getAudioSessionId();
						}

						@Override
						public boolean canSeekForward() {
							return true;
						}

						@Override
						public boolean canSeekBackward() {
							return true;
						}

						@Override
						public boolean canPause() {
							return true;
						}
					});
			mediaController.setAnchorView(imageView);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void previewVideo(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) viewGroup.findViewById(R.id.current_content);
		layout.removeAllViews();
		layout.addView(videoView);
		String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mediaLocation;
		File video = new File(fileName);
		videoView.setVideoURI(Uri.fromFile(video));
		mediaController = new MediaController(getActivity());
		mediaController.setAnchorView(videoView);
		videoView.setMediaController(mediaController);
		videoView.start();
	}

	@SuppressWarnings("deprecation")
	public void previewAnimation(String mediaLocation) {
		stopAllMedia();
		LinearLayout layout = (LinearLayout) viewGroup.findViewById(R.id.current_content);
		layout.removeAllViews();
		layout.addView(webView);
		String fileName = Environment.getExternalStorageDirectory() + "/" + mediaLocation;
		webView.getSettings().setPluginState(PluginState.ON);
		webView.loadUrl("file:///" + fileName);
		webView.getLayoutParams().height = LayoutParams.MATCH_PARENT;
		webView.getLayoutParams().width = LayoutParams.MATCH_PARENT;
	}

	public void showController() {
		if (mediaController != null) {
			mediaController.show();
		}
	}

	private void loadAssets() {
		try {
			image = BitmapFactory.decodeStream(getActivity().getAssets().open("example.jpg"));
			soundIcon = BitmapFactory.decodeStream(getActivity().getAssets().open(
					"soundicon.jpg"));
		} catch (IOException e) {
			return;
		}
	}

	private void stopAllMedia() {
		if (mediaPlayer.isPlaying())
			mediaPlayer.stop();
		webView.loadUrl("");
	}
	
	private void showImage(String mediaLocation) {
		stopAllMedia();
		Intent intent = new Intent(getActivity().getApplicationContext(), PresentationActivity.class);
		intent.putExtra("mode", "image");
		intent.putExtra("media_location", mediaLocation);
		startActivity(intent);
	}

	private void showVideo(String mediaLocation) {
		stopAllMedia();
		Intent intent = new Intent(getActivity().getApplicationContext(), PresentationActivity.class);
		intent.putExtra("mode", "video");
		intent.putExtra("media_location", mediaLocation);
		startActivity(intent);
	}

	private void showAudio(String mediaLocation) {
		stopAllMedia();
		Intent intent = new Intent(getActivity().getApplicationContext(), PresentationActivity.class);
		intent.putExtra("mode", "audio");
		intent.putExtra("media_location", mediaLocation);
		startActivity(intent);
	}

	private void showAnimation(String mediaLocation) {
		stopAllMedia();
		Intent intent = new Intent(getActivity().getApplicationContext(), PresentationActivity.class);
		intent.putExtra("mode", "animation");
		intent.putExtra("media_location", mediaLocation);
		startActivity(intent);
	}

	private void showPPT(String mediaLocation){
		stopAllMedia();
		//TODO: How to open PPT using intent
		File file = new File(Environment.getExternalStorageDirectory(), mediaLocation);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),"application/vnd.ms-powerpoint");
		startActivity(intent);
	}
	
	private void showPDF(String mediaLocation){
		stopAllMedia();
		//TODO: How to open PDF using intent
		File file = new File(Environment.getExternalStorageDirectory(), mediaLocation);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),"application/pdf");
		startActivity(intent);
	}
}
