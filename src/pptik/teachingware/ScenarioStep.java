package pptik.teachingware;

public class ScenarioStep {

	private String description;
	private String mediaLocation;
	private MediaType mediaType;
	
	public ScenarioStep(){
		description = "Guru menyapa murid";
		mediaLocation = "";
		mediaType = MediaType.NONE;
	}
	
	public ScenarioStep(String description, String mediaLocation, MediaType mediaType){
		this.description = description;
		this.mediaLocation = mediaLocation;
		this.mediaType = mediaType;
	}
	
	public MediaType getMediaType(){
		return mediaType;
	}
	
	public void setMediaType(MediaType mediaType){
		this.mediaType = mediaType;
	}
	
	public String getMediaLocation(){
		return mediaLocation;
	}
	
	public void setMediaLocation(String mediaLocation){
		this.mediaLocation = mediaLocation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
	public static enum MediaType{
		NONE, IMAGE, AUDIO, VIDEO, ANIMATION, PPT, PDF;
		
		public static MediaType parseMediaType(String mediaType){
			if(mediaType.compareTo("image") == 0){
				return IMAGE;
			}else if(mediaType.compareTo("audio") == 0){
				return AUDIO;
			}else if(mediaType.compareTo("video") == 0){
				return VIDEO;
			}else if(mediaType.compareTo("animation") == 0){
				return ANIMATION;
			}else if(mediaType.compareTo("ppt") == 0){
				return PPT;
			}else if(mediaType.compareTo("pdf") == 0){
				return PDF;
			}else{
				return NONE;
			}
		}
	}
	
}
