package pptik.teachingware;

import java.io.IOException;

import org.json.JSONException;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;

public class ScreenSlideActivity extends FragmentActivity {
	
	/**
	* The Content
	*/
	private TeachingMaterial content;
	
	/**
	* The number of pages (wizard steps) to show in this demo.
	*/
	private static final int NUM_PAGES = 2;
	
   	/**
   	 * The pager widget, which handles animation and allows swiping horizontally to access previous
   	 * and next wizard steps.
   	 */
   	private ViewPager mPager;
   	
   	/**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen_slide);
		
		// Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(this.getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });
        
        try {
			content = TeachingMaterial.read("tes.txt");
		} catch (JSONException e) {
			Log.e("cekParam", e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("cekParam", e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.screen_slide, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		if(mPager.getCurrentItem() == 0){
			super.onBackPressed();
		}else{
			mPager.setCurrentItem(mPager.getCurrentItem() - 1);
		}
	}
	
	
	/**
	 * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment} objects, in
	 * sequence.
	 */
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
	    public ScreenSlidePagerAdapter(FragmentManager fm) {
	        super(fm);
	    }

	    @Override
	    public Fragment getItem(int position) {
	    	if(position == 0){
	    		IdentityFragment fragment = new IdentityFragment();
	    		fragment.setContent(content);
	    		return fragment;
	    	}else{
	    		ScenarioFragment fragment = new ScenarioFragment();
	    		fragment.setContent(content);
	    		return fragment;
	    	}
	    }

	    @Override
	    public int getCount() {
	        return NUM_PAGES;
	    }
	}

}
