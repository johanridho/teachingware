package pptik.teachingware;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Environment;

import pptik.teachingware.ScenarioStep.MediaType;

public class TeachingMaterial {
	private HashMap<IdentityInformation, String> identity;
	private ArrayList<ScenarioStep> scenario;
	private String directory;

	public TeachingMaterial() {
		identity = new HashMap<IdentityInformation, String>();
		// Initialize all identity information with not available.
		for (IdentityInformation key : IdentityInformation.values()) {
			identity.put(key, "N/A");
		}
		scenario = new ArrayList<ScenarioStep>();
	}

	public void setIdentityField(IdentityInformation key, String value) {
		identity.remove(key);
		identity.put(key, value);
	}

	public String getIdentityField(IdentityInformation key) {
		return identity.get(key);
	}

	public void addScenarioStep(ScenarioStep step) {
		scenario.add(step);
	}

	public int getCountStep() {
		return scenario.size();
	}

	public ScenarioStep getStep(int i) {
		return scenario.get(i);
	}
	
	public String getDirectory(){
		return directory;
	}
	
	public void setDirectory(String directory){
		this.directory = directory;
	}

	public static TeachingMaterial read(String filename) throws JSONException,
			IOException {
		
		TeachingMaterial result = new TeachingMaterial();

		//read from JSON File
		File fl = new File(Environment.getExternalStorageDirectory(), filename);
		FileInputStream fin = new FileInputStream(fl);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		reader.close();
		String json = sb.toString();
		//close stream
		fin.close();

		// String json =
		// "{'id':20,'group_id':2,'status':0,'title':'Game Development Life Cycle','description':'Tahap-tahap membuat game dan daur hidup sebuah proyek pengembangan game.','school':'SD Pengangguran','topic':'Social Science','theme':'Computer','grade':5,'semester':2,'steps':4,'created_at':'2014-10-14 08:34:50','updated_at':'2014-10-14 08:34:50','authors':[{'id':2,'group_id':2,'username':'cesarb','email':'cesar.bryan@example.com','full_name':'Cesar Bryan','role':'user','created_at':'2014-10-09 06:49:02','updated_at':'2014-10-09 06:49:02'},{'id','group_id':2,'username':'greggk','email':'gregg.king@example.com','full_name':'Gregg King','role':'user','created_at':'2014-10-09 06:49:02','updated_at':'2014-10-09 06:49:02'}],'activities':[{'id':30,'material_id':20,'resource_id':0,'index_no':1,'description':'<p>Salam, buka, dan jelaskan pentingnya mengapa ada proyek game.</p>\r\n','created_at':'2014-10-14 08:34:50','updated_at':'2014-10-14 08:34:50'},{'id':31,'material_id':20,'resource_id':0,'index_no':2,'description':'<p>Sebuah proyek game dilakukan dengan pembuatan grup dan brainstorming. Detil dapat dilihat pada presentasi resource.</p>\r\n','created_at':'2014-10-14 08:34:50','updated_at':'2014-10-14 08:34:50'},{'id':32,'material_id':20,'resource_id':0,'index_no','description':'<p>Berikan PR membuat game. Rencana pengembangan dikumpulkan minggu depan.</p>\r\n','created_at':'2014-10-14 08:34:50','updated_at':'2014-10-14 08:34:50'}]}";
		// String json =
		// "{'id':20,'group_id':2,'status':0,'title':'Game Development Life Cycle','description':'Tahap-tahap membuat game dan daur hidup sebuah proyek pengembangan game.','school':'SD Pengangguran','topic':'Social Science','theme':'Computer','grade':5,'semester':2}";
		JSONObject obj = new JSONObject(json);
		// id = obj.getString("id");
		// group_id = obj.getString("group_id");
		// status = obj.getString("status");
		result.setIdentityField(IdentityInformation.TITLE,
				obj.getString("title"));
		result.setIdentityField(IdentityInformation.DESCRIPTION,
				obj.getString("description"));
		result.setIdentityField(IdentityInformation.SCHOOL,
				obj.getString("school"));
		result.setIdentityField(IdentityInformation.TOPIC,
				obj.getString("topic"));
		result.setIdentityField(IdentityInformation.THEME,
				obj.getString("theme"));
		result.setIdentityField(IdentityInformation.CLASS_SEMESTER,
				obj.getString("grade") + "/" + obj.getString("semester"));
		// description = obj.getString("description");
		// school = obj.getString("school");
		// topic = obj.getString("topic");
		// theme = obj.getString("theme");
		// grade = obj.getString("grade");
		// semester = obj.getString("semester");
		// steps = obj.getString("steps");
		// created_at = obj.getString("created_at");
		// updated_at = obj.getString("updated_at");
		
		JSONArray array = obj.getJSONArray("activities");
		for (int i = 0; i < array.length(); i++) { 
			// TODO: tambah nama file & tipe media di json.
			result.addScenarioStep(new ScenarioStep(array.getJSONObject(i).getString("description"),array.getJSONObject(i).getString("resource_id"), MediaType.parseMediaType(array.getJSONObject(i).getString("media_type"))));
		}
		return result;
	}

	public static enum IdentityInformation {
		TITLE("title"), // judul
		DESCRIPTION("description"), // deskripsi
		SCHOOL("school"), // satuan pendidikan
		TOPIC("topic"), // tema
		THEME("theme"), // subtema
		CLASS_SEMESTER("class_semester") // kelas/semester (from "grade" and
											// "semester")
		;

		private final String name;

		private IdentityInformation(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
}
