package pptik.teachingware;

import android.os.Bundle;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class WelcomePageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome_page);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome_page, menu);
		return true;
	}
	
	public void openStore(View v){
		Intent intent = new Intent("android.intent.action.MAIN");
		intent.setComponent(ComponentName.unflattenFromString("com.android.project.studio9"));
//		intent.setComponent(ComponentName.unflattenFromString("com.android.project.studio9/com.android.project.studio9.HomePortrait"));
		intent.addCategory("android.intent.category.LAUNCHER");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	public void openApp(View v){
		Intent intent = new Intent(this, ScreenSlideActivity.class);
		this.startActivity(intent);
	}

}
